let url = 'https://api.github.com/';


function search() {

    let x = document.getElementById('contributors').value;
    let pseudo = document.getElementById('user_name').value;

    let url2 = url + 'users/' + pseudo;

    if (!x && !pseudo) {
        (alert("M'enfin ! donne moi au moins une information ..."))
    }
    else {
        // recherche par contributions
        fetch(url +'users')
            .then(response => {
                return response.json();
            })
            .then(data => {
                let avatar = data[x-1].avatar_url;
                let name = data[x-1].login;
                let homePage = data[x-1].html_url;

                document.getElementById('avatar1').innerHTML = '<img src=" ' + avatar + ' " alt="" srcset="">';
                document.getElementById('login1').innerHTML = name;
                document.getElementById('homepage1').innerHTML = '<a target="_blank" href="' + homePage + '">EspaceGit</a>';
            });

        // recherche par pseudo (login)    
        fetch(url2)
            .then(response => {
                return response.json();
            })
            .then(data1 => {
                let avatar = data1.avatar_url;
                let login = data1.login;
                let name = data1.name;
                let repos = data1.public_repos;
                let location = data1.location;

                document.getElementById('avatar').innerHTML = '<img src=" ' + avatar + ' " alt="" srcset="">';
                document.getElementById('login').innerHTML = login;
                document.getElementById('name').innerHTML = name;
                document.getElementById('repos').innerHTML = repos;
                document.getElementById('location').innerHTML = location;
            });
    }
}
